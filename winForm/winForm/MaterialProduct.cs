﻿using LinqToDB.Mapping;
using System.Linq;

namespace winForm
{//MessageBox.Show("Воробьев Крикунов");
    [Table]
    class MaterialProduct
    {
        [PrimaryKey, NotNull]
        public string Id { get; set; }

        [Column]
        public int MaterialId { get; set; }

        [Column]
        public int ProductId { get; set; }

        [Column]
        public int Amount { get; set; }

        public Material Material
        {
            get
            {
                return Db.Instance.Material.Where(m => m.Id == MaterialId).FirstOrDefault();
            }
        }

        public Product Product
        {
            get
            {
                return Db.Instance.Product.Where(m => m.Id == ProductId).FirstOrDefault();
            }
        }

    }
}
