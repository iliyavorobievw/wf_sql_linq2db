﻿using LinqToDB;

namespace winForm
{
    class Db : LinqToDB.Data.DataConnection
    {
        private static Db _db;
        public static Db Instance
        {
            get
            {
                if (_db == null)
                {
                    _db = new Db($@"Data Source=DESKTOP-NJT6IKD\SQLEXPRESS;Initial Catalog=Fabrica;User ID=sa;Password=111");
                }
                return _db;
            }
        }


        private Db(string connection) : base(ProviderName.SqlServer2017, connection)
        {

        }

        public ITable<Material> Material => GetTable<Material>();
        public ITable<MaterialProduct> MaterialProduct => GetTable<MaterialProduct>();

        public ITable<MaterialType> MaterialType => GetTable<MaterialType>();

        public ITable<Product> Product => GetTable<Product>();

        public ITable<ProductType> ProductType => GetTable<ProductType>();

    }
}
