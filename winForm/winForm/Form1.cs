﻿using LinqToDB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace winForm
{
    public partial class vorobiev_krikunov : Form
    {
        private Db _db = Db.Instance;

        private string _nameSearch = "";

        private bool _orderAsc = true;

        public vorobiev_krikunov()
        {
            InitializeComponent();
        }

        private void bImport_Click(object sender, EventArgs e)
        {
            //startParse();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            MessageBox.Show("Воробьев Крикунов");

            var productTypeQuery = from pt in _db.ProductType
                                   select pt.Name;

            cBTypeProduct.Items.Add("Все");
            cBTypeProduct.Items.AddRange(productTypeQuery.ToArray());
            cBTypeProduct.SelectedIndex = 0;

            comboBox1.Items.AddRange(new[] { "Наименование", "Номер производственного цеха", "Минимальная стоимость" });
            comboBox1.SelectedIndex = 0;

            update();


        }

        private void startParse()
        {
            _db.MaterialProduct.Delete();
            _db.Material.Delete();
            _db.Product.Delete();
            _db.MaterialType.Delete();
            _db.ProductType.Delete();

            parseProduct();
            parseMaterials();
            parseMaterialProduct();

            MessageBox.Show("Done");
        }

        private void parseProduct()
        {
            var productsLines = File.ReadAllLines(@"продукты-unicode.txt");

            var productTypes = productsLines
                .Select(line => line.Split('\t'))
                .Select(item => item.Select(s => s.Trim()).ToList())
                .Select(item => item[4])
                .GroupBy(item => item)
                .Select(item => item.Key)
                .ToList();
            createProductTypes(productTypes);

            for (int i = 1; i < productsLines.Length; i++)
            {
                string line = productsLines[i];
                var item = line.Split('\t').Select(s => s.Trim()).ToList();
                var productType = _db.ProductType.Where(mt => mt.Name == item[4]).FirstOrDefault();
                var product = new Product
                {
                    Id = i,
                    Name = item[0],
                    MinAgentPrice = decimal.Parse(item[2]),
                    ProductTypeId = productType.Id,
                    NumPeople = int.Parse(item[5]),
                    FabricaName = item[6],
                    Image = item[3][0] != '\\' ? null : File.ReadAllBytes("." + item[3]),
                    Description = item[1],
                };
                _db.Insert(product);
            }
        }

        private void createProductTypes(List<string> types)
        {
            for (int i = 1; i < types.Count; i++)
            {
                string type = types[i];
                var typeMaterial = new ProductType
                {
                    Id = i,
                    Name = type
                };
                _db.Insert(typeMaterial);
            }
        }

        private void parseMaterials()
        {
            var materialsLines = File.ReadAllLines(@"материалы-unicode.txt");

            var materialTypes = materialsLines
                .Select(line => line.Split('\t'))
                .Select(item => item.Select(s => s.Trim()).ToList())
                .Select(item => item[1])
                .GroupBy(item => item)
                .Select(item => item.Key)
                .ToList();
            createMaterialTypes(materialTypes);

            for (int i = 1; i < materialsLines.Length; i++)
            {
                string line = materialsLines[i];
                var item = line.Split('\t').Select(s => s.Trim()).ToList();
                var name = item[0];
                var nameArr = name.Split(' ');
                var product = new Material
                {
                    Id = i,
                    Name = name,
                    MaterialTypeId = _db.MaterialType.Where(mt => mt.Name == item[1]).FirstOrDefault().Id,
                    AmountPackage = int.Parse(item[2]),
                    Unit = item[3],
                    AmountWarehouse = int.Parse(item[4]),
                    MinAmount = int.Parse(item[5]),
                    Price = decimal.Parse(item[6]),
                    Size = nameArr.Last(),
                    StandartType = string.Join(" ", nameArr.Where((_, index) => index >= 1 && index < nameArr.Length - 1))
                };
                _db.Insert(product);
            }
        }

        private void createMaterialTypes(List<string> types)
        {
            for (int i = 1; i < types.Count; i++)
            {
                string type = types[i];
                var typeMaterial = new MaterialType
                {
                    Id = i,
                    Name = type
                };
                _db.Insert(typeMaterial);
            }
        }

        private void parseMaterialProduct()
        {
            var materialProductLines = File.ReadAllLines(@"продукты_к_материалам_импорт.txt");

            for (int i = 1; i < materialProductLines.Length; i++)
            {
                string line = materialProductLines[i];
                var item = line.Split('\t').Select(s => s.Trim()).ToList();
                var product = new MaterialProduct
                {
                    Id = $"{i}",
                    ProductId = _db.Product.Where(m => m.Name == item[0]).FirstOrDefault().Id,
                    MaterialId = _db.Material.Where(m => m.Name == item[1]).FirstOrDefault().Id,
                    Amount = int.Parse(item[2]),
                };
                _db.Insert(product);
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void cBTypeProduct_SelectedIndexChanged(object sender, EventArgs e)
        {
            update();
        }

        private void update()
        {

            var productMaterialLinq = from mp in _db.MaterialProduct
                                      from p in _db.Product.InnerJoin(p => p.Id == mp.ProductId)
                                      from pt in _db.ProductType.InnerJoin(pt => pt.Id == p.ProductTypeId)
                                      where p.Name.Contains(_nameSearch)
                                      select new { mp, p, pt };
            switch (comboBox1.SelectedIndex)
            {
                case 0:
                    productMaterialLinq = _orderAsc ? productMaterialLinq.OrderBy(mp => mp.p.Name) : productMaterialLinq.OrderByDescending(mp => mp.p.Name);
                    break;
                case 1:
                    productMaterialLinq = _orderAsc ? productMaterialLinq.OrderBy(mp => mp.p.FabricaName) : productMaterialLinq.OrderByDescending(mp => mp.p.FabricaName);
                    break;
                case 2:
                    productMaterialLinq = _orderAsc ? productMaterialLinq.OrderBy(mp => mp.p.MinAgentPrice) : productMaterialLinq.OrderByDescending(mp => mp.p.MinAgentPrice);
                    break;
            }
            if (cBTypeProduct.Text != "Все")
            {
                productMaterialLinq = productMaterialLinq.Where(mp => mp.pt.Name == cBTypeProduct.Text);
            }
            var productMaterial = productMaterialLinq.Select(mp => mp.mp).ToList();

            dataGridView.Rows.Clear();
            for (int i = 0; i < productMaterial.Count; i++)
            {
                var item = productMaterial[i];
                dataGridView.Rows.Add();

                var productType = _db.ProductType.Where(pt => pt.Id == item.Product.ProductTypeId).FirstOrDefault();
                var materialType = _db.MaterialType.Where(mt => mt.Id == item.Material.MaterialTypeId).FirstOrDefault();

                dataGridView.Rows[i].Height = 250;
                dataGridView.Rows[i].Cells[0].Value = item.Product.Image;
                dataGridView.Rows[i].Cells[1].Value = item.Product.Name;
                dataGridView.Rows[i].Cells[2].Value = item.Product.Description;
                dataGridView.Rows[i].Cells[3].Value = item.Product.MinAgentPrice;
                dataGridView.Rows[i].Cells[4].Value = productType.Name;
                dataGridView.Rows[i].Cells[5].Value = materialType.Name;
            }

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            _nameSearch = (sender as TextBox).Text;
            update();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            update();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _orderAsc = true;
            update();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            _orderAsc = false;
            update();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
    }
}
