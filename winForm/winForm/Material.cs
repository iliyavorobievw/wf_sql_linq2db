﻿using LinqToDB.Mapping;

namespace winForm
{
    //Data Source=K28-4\SQLEXPRESS;Initial Catalog=Fabrica;Persist Security Info=True;User ID=sa;Password=111


    //MessageBox.Show("Воробьев Крикунов");
    [Table (Name = "Material")]
    class Material
    {
        [PrimaryKey,NotNull]
        public int Id { get; set; }//

        [Column]
        public string Name { get; set; }

        [Column]
        public int MaterialTypeId { get; set; }

        [Column]
        public int AmountPackage { get; set; }

        [Column]
        public string Unit { get; set; }

        [Column]
        public int AmountWarehouse { get; set; }

        [Column]
        public int MinAmount { get; set; }

        [Column]
        public decimal Price { get; set; }

        [Column]
        public string Size { get; set; }

        [Column]
        public string StandartType { get; set; }

    }

    //Воробьев Крикунов
}
