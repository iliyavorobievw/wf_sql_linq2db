﻿using LinqToDB.Mapping;

namespace winForm
{
    [Table]
    class MaterialType
    {
        [PrimaryKey]
        public int Id { get; set; }

        [Column]
        public string Name { get; set; }


    }

}
