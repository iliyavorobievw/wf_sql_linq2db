﻿using LinqToDB.Mapping;

namespace winForm
{
    [Table]
    class Product
    {

        [PrimaryKey]
        public int Id { get; set; }//

        [Column]
        public string Name { get; set; }

        [Column]
        public decimal MinAgentPrice { get; set; }

        [Column]
        public int ProductTypeId { get; set; }

        [Column]
        public int NumPeople { get; set; }

        [Column]
        public string FabricaName { get; set; }

        [Column]
        public byte[] Image { get; set; }

        [Column]
        public string Description { get; set; }


    }
}//MessageBox.Show("Воробьев Крикунов");
